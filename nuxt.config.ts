// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  css: [
    '~/assets/css/main.scss'
  ],
  modules: [
    '@nuxtjs/tailwindcss',
  ],
  app: {
    head: {
      title: 'Et ton maire ?!',
      meta: [
        { charset: 'utf-8' },
        {
          hid: 'description',
          name: 'description',
          content: `Et ton ou ta maire, il en pense quoi ? La plateforme citoyenne qui recense les avis des maires sur la réforme des retraites`,
        },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { name: 'format-detection', content: 'telephone=no' },
        { hid: 'og-type', property: 'og:type', content: 'website' },
        { hid: 'og-title', property: 'og:title', content: 'Et ton maire ?!' },
        { hid: 'og-desc', property: 'og:description', content: 'Et ton ou ta maire, il en pense quoi ? La plateforme citoyenne qui recense les avis des maires sur la réforme des retraites' },
        { hid: 'og-image', property: 'og:image', content: 'https://et-ton-maire.fr/card.png'},
        { hid: 't-type', name: 'twitter:card', content: 'summary_large_image' },
        { hid: 't-type', name: 'twitter:site', content: '@et_ton_maire' },
        { hid: 't-title', name: 'twitter:title', content: 'Et ton maire ?!' },
        { hid: 't-desc', name: 'twitter:description', content: 'Et ton ou ta maire, il en pense quoi ? La plateforme citoyenne qui recense les avis des maires sur la réforme des retraites' },
        { hid: 't-image', name: 'twitter:image', content: 'https://et-ton-maire.fr/card.png' },
        { hid: 't-url', name: 'twitter:url', content: 'https://et-ton-maire.fr/' },
      ],
      link: [
        {rel: 'icon', type: 'image/x-icon', href: '/favicon.svg' }
      ],
      viewport: 'width=500, initial-scale=1',
      script: [
        {
          src: 'https://gc.zgo.at/count.js',
          async: true ,
          'data-goatcounter': 'https://et-ton-maire.goatcounter.com/count',
        }
      ],
    },
  },
  components: true,
  runtimeConfig: {
    public: {
      lckBasePath: 'https://locokit.makina-corpus.net/api', // see NUXT_PUBLIC_LCK_BASE_PATH in .env
      lckDbUuid: '', // see NUXT_PUBLIC_LCK_DB_UUID in .env
      lckUsername: '', // see NUXT_PUBLIC_LCK_USERNAME in .env
      lckPassword: '' // see NUXT_PUBLIC_LCK_PASSWORD in .env
    }
  }
})
